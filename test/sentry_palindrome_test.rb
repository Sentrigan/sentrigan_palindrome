require "test_helper"

class SentryPalindromeTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::SentryPalindrome::VERSION
  end

  def test_non_palindrome
    refute "apple".palindrome?
  end

  def test_palindrome_simple
    assert "racecar".palindrome?
  end

  def test_mixedcase_palindrome
    assert "RaceCar".palindrome?
  end

  def test_palindrome_nonletter
    assert "Madam, I'm Adam".palindrome?
  end
end
