#require "sentry_palindrome/version"

class String

  def palindrome?
    return processed_content == processed_content.reverse
  end

  private

  def processed_content
    return self.scan(/[a-z]/i).join.downcase
  end
end
